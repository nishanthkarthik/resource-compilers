% Preamble
\documentclass[169]{beamer}
\usetheme{metropolis}

% Packages
\usepackage{amsmath}
\usepackage{shellesc}
\usepackage[outputdir=../auxil]{minted}
\usepackage{xcolor}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{tikz}

\title{Resource compilation}
\subtitle{\url{gitlab.com/nishanthkarthik/honeycomb}}
\date{\today}
\author{Karthik Nishanth}
\institute{Zivid}

% Patches
\newcommand{\betweenminted}[1]{%
    \par\vspace{-\medskipamount}\vspace{-2\baselineskip}%
    \vspace{#1}%
}

% Global configuration
\metroset{block=fill}
\definecolor{srcbg}{rgb}{0.95,0.95,0.95}
\setminted{fontsize=\tiny,autogobble,bgcolor={srcbg}}

\usetikzlibrary{shapes.geometric, arrows, positioning}
\tikzstyle{process} = [rectangle, rounded corners, text centered, draw=black]
\tikzstyle{arrow}=[draw, -latex]
\tikzstyle{dottedarrow}=[draw, dashed, ->, -latex]

% Document
\begin{document}

    \maketitle


    \section{Resources} %-----------------------------------------------------

    \subsection{Why compile resources?}
    \begin{frame}{\subsecname}
        \begin{itemize}
            \item Use a binary blob at runtime
            \item Does not require a file system
            \item On-the-fly decompression
        \end{itemize}
    \end{frame}

    \subsection{How zivid-sdk compiles resources}
    \begin{frame}{\subsecname}
        \begin{center}
            \begin{tikzpicture}[node distance=2cm]
                \node (p1) [process] {Blob};
                \node (p2) [process, below of=p1] {C++ source};
                \node (p3) [process, below of=p2] {Static library};
                \draw [arrow] (p1) -- node[anchor=east] {hexdump} (p2);
                \draw [arrow] (p2) -- node[anchor=east, fill=red!20, rounded corners] {gcc} (p3);
            \end{tikzpicture}
        \end{center}
    \end{frame}

    \subsection{How to make it fast}
    \begin{frame}{\subsecname}
        \begin{center}
            \begin{tikzpicture}[node distance=2cm]
                \node (p1) [process] {Blob};
                \node (p2) [process, below of=p1] {C++ source};
                \node (p3) [process, below of=p2] {Static library};
                \node (p4) [process, right=2cm of p2, fill=green!20] {\textit{magic sauce}};
                \draw [dottedarrow] (p1) -- node[anchor=east] {hexdump} (p2);
                \draw [dottedarrow] (p2) -- node[anchor=east] {gcc} (p3);
                \draw [arrow] (p1) -| (p4);
                \draw [arrow] (p4) |- (p3);
            \end{tikzpicture}
        \end{center}
    \end{frame}


    \section{Object files} %---------------------------------------------------

    \subsection{Introduction}
    \begin{frame}{\subsecname}
        \begin{itemize}
            \item Only dealing with data
            \item No executable or relocatable code
            \item GCC specific
            \item Object files are ELF files
        \end{itemize}
    \end{frame}

    \subsection{GCC's representation}
    \begin{frame}[fragile]{\subsecname}
        \inputminted{c}{../../src/integers.c}
        \betweenminted{0.1in}
        \inputminted[lastline=15,highlightlines={1,5}]{text}{../../src/integers.hexdump.txt}
    \end{frame}

    \subsection{Sections}
    \begin{frame}[fragile]{\subsecname}
        \inputminted{c}{../../src/integers.c}
        \betweenminted{0.1in}
        \inputminted[highlightlines={7,8}]{text}{../../src/integers.sections.txt}
    \end{frame}

    \begin{frame}{\subsecname}
        \textbf{HAS\_CONTENTS: } The section has contents - a data section could be ALLOC | HAS\_CONTENTS
        \medskip \\
        \textbf{ALLOC: } Tells the OS to allocate space for this section when loading
        \medskip \\
        \textbf{LOAD: } Tells the OS to load the section from the file when loading
        \medskip \\
        \textbf{DATA: } The section contains data only
        \medskip \\
        \textbf{READONLY: } A signal to the OS that the section contains read only data
    \end{frame}

    \subsection{Symbol table}
    \begin{frame}[fragile]{\subsecname}
        \inputminted{c}{../../src/integers.c}
        \betweenminted{0.1in}
        \inputminted[highlightlines={5-6,10}]{text}{../../src/integers.objdump.txt}
    \end{frame}

    \subsection{Arrays}
    \begin{frame}[fragile]{\subsecname}
        \inputminted{c}{../../src/pointers.c}
        \betweenminted{0.1in}
        \inputminted[lastline=15,highlightlines={5,6}]{text}{../../src/pointers.hexdump.txt}
    \end{frame}

    \subsection{Alignment}
    \begin{frame}[fragile]{\subsecname}
        \inputminted{c}{../../src/pointers.c}
        \betweenminted{0.1in}
        \inputminted[highlightlines={7-8}]{text}{../../src/pointers.sections.txt}
    \end{frame}

    \subsection{Symbol table}
    \begin{frame}[fragile]{\subsecname}
        \inputminted{c}{../../src/pointers.c}
        \betweenminted{0.1in}
        \inputminted[highlightlines={5-6}]{text}{../../src/pointers.objdump.txt}
    \end{frame}

    \subsection{GNU bfd}
    \begin{frame}{\subsecname}
        \begin{quote}
            \footnotesize
            The name came from a conversation David Wallace was having with Richard
            Stallman about the library: RMS said that it would be quite hard -- David
            said "BFD". Stallman was right, but the name stuck.
        \end{quote}
        \begin{itemize}
            \item Written by Cygnus (RedHat) for Intel
            \item Part of binutils
            \item ld.bfd, the GNU linker
            \item abstraction over objects \& libraries
            \item 414 architectures + 257 targets
        \end{itemize}
    \end{frame}

    \subsection{Implementation notes}
    \begin{frame}[fragile]{\subsecname}
        \begin{itemize}
            \item Memory mapped files
            \item Endian-ness
        \end{itemize}

        \begin{minted}{c}
            #define bfd_put(bits, abfd, val, ptr)                  \
              ((bits) == 8 ? bfd_put_8  (abfd, val, ptr)           \
               : (bits) == 16 ? bfd_put_16 (abfd, val, ptr)        \
               : (bits) == 32 ? bfd_put_32 (abfd, val, ptr)        \
               : (bits) == 64 ? bfd_put_64 (abfd, val, ptr)        \
               : (abort (), (void) 0))
        \end{minted}
    \end{frame}


    \section{Cross compilation} %---------------------------------------------------

    \subsection{Toolchains}
    \begin{frame}{\subsecname}
        \begin{tabular}{rccccc}
            \textbf{native}         & build & \equiv      &  host & \equiv       &  target \\
            \textbf{cross}          & build & \equiv      &  host & \not\equiv   &  target \\
            \textbf{cross-native}   & build & \not\equiv  &  host & \equiv       &  target \\
            \textbf{canadian}       & build & \not\equiv  &  host & \not\equiv   &  target \\
        \end{tabular}
    \end{frame}

    \subsection{Cross compiling for windows}
    \begin{frame}{\subsecname}
        \begin{center}
            \begin{itemize}
                \item Cygwin
                \item MinGW
                \item MSYS2
            \end{itemize}
        \end{center}
    \end{frame}

    \section{Demo}
    \subsection{Just do it!}
    \begin{frame}{\subsecname}
        \begin{center}
            \includegraphics[width=\textwidth]{assets/shia}
        \end{center}
    \end{frame}
\end{document}
